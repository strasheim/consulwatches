// Package consulwatches is a small utility to read thou the services
// while fishing for TAGs. Currently only monitor and ASGtrigger is looked for
// It will create a valid json file for consul to consume
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"log/syslog"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"syscall"
)

// Watches an array for a consul service watch.
type Watches struct {
	Watches []Watch `json:"watches"`
}

// Watch is a single struct that matches a consul server watch.
type Watch struct {
	Type    string `json:"type"`
	Service string `json:"service"`
	Handler string `json:"handler"`
}

func check(err error) {
	if err != nil {
		log.Panicln(err)
	}
}

func readServices() (map[string][]string, error) {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", "http://127.0.0.1:8500/v1/catalog/services", nil)
	if len(os.Getenv("TOKEN")) > 1 {
		req.Header.Set("X-Consul-Token", os.Getenv("TOKEN"))
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == 200 {
		content, errr := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()
		var objmap map[string][]string
		errr = json.Unmarshal(content, &objmap)
		return objmap, errr
	}
	return nil, err

}

func main() {
	logwriter, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	check(err)
	toFile := false
	reloadConsul := false

	if len(os.Args) > 1 {
		toFile = true
	}
	if len(os.Args) > 2 {
		reloadConsul = true
	}

	search, binary := defaultsByName(os.Args[0])

	log.SetOutput(logwriter)
	services, err := readServices()
	check(err)
	var watches []Watch
	var data Watch
	for service, tags := range services {
		monitor := false
		for _, tag := range tags {
			if tag == search {
				monitor = true
			}
		}
		if monitor {
			data = Watch{Type: "service", Service: service, Handler: binary}
			watches = append(watches, data)
		}
	}

	printme := Watches{Watches: watches}
	file, _ := json.MarshalIndent(printme, "", "  ")
	if toFile {
		data := []byte(file)
		olddata, err := ioutil.ReadFile(os.Args[1])
		if err == nil && len(data) == len(olddata) {
			reloadConsul = false
		}
		if reloadConsul {
			log.Println("New consul watch file generated")
			err = ioutil.WriteFile(os.Args[1], data, 0644)
			check(err)
		}
	} else {
		fmt.Println(string(file))
		os.Exit(0)
	}

	if reloadConsul {
		log.Println("reloading consul")
		binary, lookErr := exec.LookPath("consul")
		if lookErr != nil {
			panic(lookErr)
		}
		args := []string{"consul", "reload"}
		env := os.Environ()
		execErr := syscall.Exec(binary, args, env)
		if execErr != nil {
			panic(execErr)
		}
	}
}

func defaultsByName(name string) (tag string, binary string) {

	nameParts := strings.Split(name, "/")
	myName := nameParts[len(nameParts)-1]

	switch myName {
	case "consulwatches":
		tag = "monitor"
		binary = "/usr/local/bin/consul2pd"
		break
	case "asgwatches":
		tag = "ASGtrigger"
		binary = "/usr/local/bin/consul4asg"
		break
	}
	return
}
