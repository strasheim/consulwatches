
## ConsulWatches  

Every service that has the tag monitor will get a watch file generated, which will trigger /usr/local/bin/consul2pd for all the checks it has.If there are no checks and just the tag, the watch is still generated but will never fire. As such you need to create the checks for the service yourself. 


#### How to install

1. set GOPATH
2. go get -u gitlab.com/strasheim/consulwatches
3. binary is under $GOPATH/bin/

#### Details 

The config json is printed to stdout, this should enable you to put it where you see fit. 
Very often consul is using /etc/consul.d/ as a config directory. 

```
/usr/local/bin/consulwatches > /etc/consul.d/watches.json 
```

Failures are printed to syslog and not stdout or stderr. Successful generation of a new file will also generate a log line. 
It's intended to be run automatically as part of system setup. 

Once the file is in the consul config directory you need to run 

```
consul reload
```

#### Integration information

This tool is only required to be run once for the consul cluster. It does not hurt to do it on all servers on the other hand. If the watches are already there the consul reload does not fail. Only non-valid syntax would make it fail. 

The tool assumes that all binaries are in /usr/local/bin  [ consul, consul2pd, consulwatches ] 

#### Example 

```
./consulwatches      
{
  "watches": [
    {
      "type": "service",
      "service": "web",
      "handler": "/usr/local/bin/consul2pd"
    }
  ]
}
```

#### Options and Defaults

While it works nicely without any parameters, there are times where it's nice to have a few options. The steps above can all be done in a sinlgle run. 

```
./consulwatches /etc/consul.d/watches.json reload 
```

This will cause consul watches to not print the output to STDOUT but to file given and reload consul once done. 
This allows for a simple watch to build service watches:

```
cat /etc/consul.d/xbuildwatches.json 
{
  "watches": [ 
    {
      "type" : "services",
      "handler": "/usr/local/bin/consulwatches /etc/consul.d/watches.json reload"
    }
  ]
}
```

With this the watches are rebuild as soon as a service change is detected by consul - the load is triggered if length of file would change.

----

If you rename consulwatches to asgwatches it will search for the tag ASGtrigger and call /usr/local/bin/consul4asg - details [here] (https://gitlab.com/strasheim/consul4asg)  

```
cat /etc/consul.d/xasgtriggers.json
{       
  "watches": [ 
    {   
      "type" : "services",
      "handler": "/usr/local/bin/asgwatches /etc/consul.d/watchesasg.json reload"
    }   
  ]     
}       
```    

### Consul Token 
consulwatches reads the ENV TOKEN and if set will use it. It only reads the HTTP service endpoing and requires read acccess to the same. 
